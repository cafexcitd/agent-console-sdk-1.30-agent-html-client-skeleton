<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.0.99:8443/assistserver/sdk/web/shared/css/shared-window.css">

    <style type="text/css">

        #local {
            width: 160px;
            height: 120px;
        }

        #remote {
            width: 320px;
            height: 240px;
        }

        #local, #remote {
            border: 1px solid grey;
        }

    </style>
</head>

<body>

    <!-- remote video view -->
    <div id="remote"></div>

    <!-- local video preview -->
    <div id="local"></div>

    <button id="end">End</button>

    <!-- libraries needed for Assist SDK -->
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
    <script src="http://192.168.0.99:8080/gateway/adapter.js"></script>
    <script src="http://192.168.0.99:8080/gateway/csdk-sdk.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/assist-aed.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/shared/js/shared-windows.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console.js"></script>
    <script src="http://192.168.0.99:8080/assistserver/sdk/web/agent/js/assist-console-callmanager.js"></script>



    <!-- load jQuery - helpful for DOM manipulation -->
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

    <!-- control -->
    <script>

        <?php
        // since we know that the agent will require to be provisioned, we
        // can eagerly provision a session token on the page load rather
        // than do so lazily later

        // include the Provisioner class we created to do the provisioning
        require_once('Provisioner.php');
        $provisioner = new Provisioner;
        $token = $provisioner->provisionAgent(
            'agent1',
            'example.com',
            '.*');

        // decode the sessionID from the provisioning token
        $sessionId = json_decode($token)->sessionid;
        ?>

        var sessionId = '<?php echo $sessionId; ?>';
        console.log("SessionID: " + sessionId);

    </script>
</body>
</html>